#coding:utf8
'''
Created on 2013-10-29

@author: ywq
'''

from app.scense.core.user.playermanager import PlayerManager
from app.scense.core.language.language import Lg
from app.mem.mPlayer import player_m

def getIntensifyInfo1401(pid,response):
    '''获取强化界面信息
    @param pid: int 角色id
    '''
    player=PlayerManager().getPlayer(pid)
    if not player:
        response.result = False
        response.message = Lg().g(1000)
        return
    response.result = True
    response.message = Lg().g(1001)
    player.getIntensifyInfo(response)
    
def intensifyPet1402(pid,petId,typeId,response):
    '''强化宠物
    @param pid: int 角色id
    @param petId: int 宠物id
    @param typeId: int 强化类型   1强化生命  2强化攻击力 3渡劫（升级品质）
    '''
    player=PlayerManager().getPlayer(pid)
    if not player:
        response.result = False
        response.message = Lg().g(1000)
        return
    money = player.silvermoney
    luck = player.luck
    newMoney,newLuck,state = player.pets[petId].intensifyPet(typeId,money,luck,response)#宠物强化
    if response.result:#如果成功进行了强化操作
        pObj = player_m.getObj(pid)
        data = {"silvermoney":newMoney,"luck":newLuck}
        pObj.update_multi(data)#更新memcached中角色的银币和幸运值
        pObj.syncDB()
        player.silvermoney = newMoney#更新角色的银币
        player.luck = newLuck#更新角色的幸运值
        if state:#强化或渡劫成功
            response.data[0].newfighting = player.countFn()#给客户端返回角色新的小队战力
