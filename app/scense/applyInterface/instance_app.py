#coding:utf8
'''
Created on 2013-10-8
副本
@author: jt
'''
from app.scense.core.user.playermanager import PlayerManager
from app.scense.core.instance.instanceManager import InstanceManager
from app.scense.core.instance.instance import Instance
from app.scense.tables import instanceOrder_table, petNewInstance_table
from app.scense.tables import instance_table
import time
from app.db import player_petdb
from app.mem.mPlayer import playerPet_m


def getInstanceInfoByid(pid,instanceid,rsp):
    '''根据角色和副本id获取副本信息
    @param pid: int 角色id
    @param instanceid: int 副本id
    @param rsp: obj   proto类
    '''
    instance = InstanceManager().add(instanceid, pid) #副本信息
    return instance.getinfo(rsp)
    
def getInstanceInfo(pid,rsp):
    '''获取角色可进行的副本信息'''
#    from app.scense.tables import instance_table
    player=PlayerManager().getPlayer(pid)
    ccid=player.ciid#副本通关id
    instanceid=0
    if ccid!=0:
        try:
            instanceid=instanceOrder_table.index(ccid)#副本id在顺序中的位置
        except:
            print 'instance_app  getInstanceInfo() %d'%ccid
            return
        if instanceid<=0 and (len(instanceOrder_table)-1)<=instanceid:
            print"已经是最后一个副本了,再也没有更新的副本"
            return
    nextInstanceId=instanceOrder_table[instanceid+1]
    instance=Instance(nextInstanceId)
    return instance.getinfo(rsp)
    
        
def getInstanceid(pid,rsp):
    '''获取角色可进行的副本id'''
    nextInstanceId=getInstanceid1(pid)
    instance=Instance(nextInstanceId)
    rsp.result=True
    rsp.instanceid=nextInstanceId
    rsp.fn=instance.fn

def getInstanceid1(pid): #这个地方后期放到角色类，当通关之后给觉得的先一个副本id改变
    player=PlayerManager().getPlayer(pid)
    ccid=player.ciid#副本通关id
    instanceid=-1
    if ccid!=0:
        try:
            instanceid=instanceOrder_table.index(ccid)#副本id在顺序中的位置
        except:
            print 'instance_app  getInstanceInfo() %d'%ccid
            return
        if instanceid>=0 and (len(instanceOrder_table)-1)<=instanceid:
            print"pid:%d  已经是最后一个副本了,再也没有更新的副本"%pid
            return ccid
    nextInstanceId=instanceOrder_table[instanceid+1]
    return nextInstanceId

def joinInstance(pid, rsp):
    '''角色请求进入副本
    @param pid: int 角色id
    '''
    player=PlayerManager().getPlayer(pid)
    instanceid=getInstanceid1(pid)#角色挑战的副本id
    instance = InstanceManager().add(instanceid, pid) #副本信息
    player.instancedid=instance.did #角色记录自己的副本动态id
    rsp.result=True
    
    
def closeInstance(pid,rsp):
    '''角色退出副本'''
    player=PlayerManager().getPlayer(pid)
    did=player.instancedid#副本动态id
    instanceInfo=InstanceManager().getByid(did)#副本实例
#    if instanceInfo.liveMonsterNumber>0 and instanceInfo.liveHeroNumber>0: #如果活着的英雄数量或者活着的怪物数量>0
#        pass
#        return
    startTime=instanceInfo.startTime #副本开始时间
    if int(time.time())-startTime<5:
        InstanceManager().drop(did)
        rsp.result=False
        return
    count=instanceInfo.silver#通关获得的银币数量
    player.addSilver(count)
    exp = instanceInfo.exp#通关获得的经验
    player.addPetsExp(exp)#给出战宠物增加经验
    player.updateCiid(instanceInfo.id) #包括解锁宠物
    InstanceManager().drop(did)
    player.assessment(1,instanceInfo.id) #
    rsp.result=True
    #检查有没有新解锁的宠物
    
    
def verification(data,rsp):
    '''副本中战斗验证'''
    pid=data.get('charid',None)
    counts=data.get('counts')
    skillId=data.get('skillId')
    player=PlayerManager().getPlayer(pid)
    did=player.instancedid#副本动态id
    instanceInfo=InstanceManager().getByid(did)#副本实例
    attid=data.get('strikerId')#攻击者id
    enemyArray=data.get('enemyArray')#战斗记录
    for item in enemyArray:
        defid=item.get('attackedId')#被攻击者id
        curhp=item.get('curhp') #被攻击前血量
        changeblood=item.get('changeblood') #改变血量
        instanceInfo.attack(attid,defid,counts,killId=skillId,harm=changeblood)
    
    
    
    
    
    