#coding:utf8
'''
Created on 2013-11-7

@author: ywq
'''

from app.scense.core.user.playermanager import PlayerManager
from app.scense.core.language.language import Lg

def getAllWelfareInfo1501(pid,rtn):
    '''获取福利界面信息
    '''
    player=PlayerManager().getPlayer(pid)
    if not player:
        return {'result':False,'message':Lg().g(1000)}
    player.welfare.getShow(rtn)
    

def getReward1502(pid,typeid):
    '''领取福利
    '''
    player=PlayerManager().getPlayer(pid)
    if not player:
        return {'result':False,'message':Lg().g(1000)}
    data = player.welfare.receiveTask(typeid)
    return data