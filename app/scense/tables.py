#coding:utf8
'''
Created on 2013-9-22
数据库中的配置信息
@author: jt
'''
from app.db import petdb,growingdb,modulusdb, skilldb, monsterdb, instancedb,\
    configsdb, pet_new_instancedb,languagedb,intensifydb,welfaredb

pet_table={}     #pet表 key:id  value：{}
monster_table={} #monster表 key:id value:{}
growing_table={} #宠物成长表
modulus_table={} #战斗系数
skill_table={}   #技能信息
instance_table={}#副本信息
configs_table={} #配置信息
petNewInstance_table={} #通关添加新宠物
language_table={}#语言表
intensify_table={}#宠物强化成功率和消耗表
instanceOrder_table=[]  #副本顺序列表 [副本id,副本id,副本id,副本id]
welfare_table={}#福利配置表所有数据
welfareOrder={} #key:福利任务类型 ，value:[welfare任务主键id,welfare任务主键id]
welfareType={} #key:类型id,value:{id:0,context:0}
pet_exp={}#宠物升级所需经验表

def sets():
    '''给变量赋值'''
    #宠物表 pet
    global pet_table
    pet_table=petdb.getAllPets()
            
    #怪物表monster
    global monster_table
    monster_table=monsterdb.getAllMonster()
            
    #宠物成长表 growing
    global growing_table
    growinfo=growingdb.getAllGrowing()
    for item in growinfo:
        growing_table[item['id']]=item
        
    #战斗系数
    global modulus_table
    modulus_table=modulusdb.getModulus()
    
    #技能信息
    global skill_table
    skill_table=skilldb.getAllskill()
    
    #副本信息
    global instance_table
    instance_table=instancedb.getAllInstance()
    
    #配置信息
    global configs_table
    configs_table=configsdb.geConfigs()
    
    #通关添加新宠物
    global petNewInstance_table
    petNewInstance_table=pet_new_instancedb.getAll()
    
    #语言表
    global language_table
    language_table = languagedb.getAllLanguage()
    
    #宠物强化成功率和消耗表
    global intensify_table
    intensify_table = intensifydb.getAllIntensifyInfo()
    
    global instanceOrder_table
    instanceOrder_table=instancedb.getInstanceIdList()
    
    #福利表
    global welfare_table
    global welfareOrder
    global welfareType
    welfareType=welfaredb.getAllTypes()
    welfareOrder=welfaredb.getOrderInfo()
    welfare_table = welfaredb.getAllIntensifyInfo()
    
    #宠物升级所需经验表
    global pet_exp
    pet_exp = petdb.getPetExp()
    
