#coding:utf8
'''
Created on 2013-10-29

@author: ywq
'''

from app.scense.component.Component import Component
from app.scense.core.pet.pet import Pet
from app.mem.mPlayer import playerPet_m,player_m
#from app.scense.tables import growing_table
from app.scense.core.language.language import Lg
import random

class Intensify(Component):
    '''强化
    '''
    def __init__(self,owner):
        Component.__init__(self,owner)
        self.luck = 0#角色宠物强化幸运值
        self.initIntensifyData()
        
    def initIntensifyData(self):
        '''初始化强化信息
        '''
        playerInfo=player_m.getObj(self._owner.id).get("data")
        self.luck=playerInfo.get('luck')#角色宠物强化幸运值
        
    def getIntensifyInfo_old(self):
        '''获取强化界面信息
        '''
        allInfo = {}
        allInfo['fighting'] = self._owner.countFn()
        allInfo['silvermoney'] = self._owner.silvermoney
        allInfo['luck'] = self.luck
        allInfo['teaminfo'] = self.getAllPetInfo()
        return allInfo
        
    def getAllPetInfo(self):
        '''获取所有小伙伴强化信息
        '''
        from app.scense.tables import intensify_table
        allPetInfo = {}
        pets = self._owner.getAllUnlockPets() 
        for pet in pets.values():
            #dbInfo = playerPet_m.getObj(i).get("data")
            petInfo = {}
            petInfo['name'] = pet.name
            petInfo['level'] = pet.qhhplv + pet.qhattlv
            petInfo['levellimit'] = pet.qhlimit
            petInfo['resource'] = pet.resourceid
            petInfo['headid'] = pet.headid
            petInfo['color'] = pet.color
            petInfo['hp'] = pet.countHp()
            petInfo['hpmoney'] = intensify_table.get(pet.qhhplv+1,{'money':0})['money']
            petInfo['att'] = pet.countAtt()
            petInfo['attmoney'] = intensify_table.get(pet.qhattlv+1,{'money':0})['money']
            petInfo['status'] = pet.upmoney
            allPetInfo[pet.ppid] = petInfo
        return allPetInfo
    
    def countAddAtt(self,gid,typeId,qhlevel):
        '''计算强化后增加的属性
        @param gid: int 宠物成长idid
        @param typeId: int 强化类型  1强化生命  2强化攻击力
        @param qhlevel: int 当前强化等级
        '''
        from app.scense.tables import configs_table
        if typeId == 1:
            formula = configs_table.get(1000)
        else:
            formula = configs_table.get(1001)
        nowAtt = eval(formula)
        qhlevel -= 1
        beforeAtt = eval(formula)
        if qhlevel < 1:
            beforeAtt = 0
        result = nowAtt - beforeAtt
        return result
    
    def countDJAdd(self,gid,qhlevel,typeId):
        '''计算渡劫后增加的属性
        @param gid: int 宠物成长idid
        @param typeId: int 要计算的属性类型  1强化生命  2强化攻击力
        '''
        from app.scense.tables import configs_table
        if typeId == 1:
            formula = configs_table.get(1000)
        else:
            formula = configs_table.get(1001)
        nowAtt = eval(formula)
        gid -= 1
        beforeAtt = eval(formula)
        result = nowAtt - beforeAtt
        return result
    
    def intensifyPet(self,petId,typeId):
        '''强化宠物
        @param petId: int 宠物id
        @param typeId: int 强化类型   1强化生命  2强化攻击力 3渡劫（升级品质）
        '''
        pets = self._owner.getAllUnlockPets()
        if petId not in pets:
            return {'result':False,'message':Lg().g(1002)}
        pet = pets[petId]
        playerInfoObj = player_m.getObj(self._owner.id)
        playerInfo = playerInfoObj.get("data")
        if typeId == 1 or typeId == 2:#强化属性
            from app.scense.tables import intensify_table,growing_table
            if typeId == 1:
                typeStr = 'qhhplv'
            else:
                typeStr = 'qhattlv'
            petInfoObj = playerPet_m.getObj(petId)
            petInfo = petInfoObj.get("data")
            growingInfo = growing_table[pet.gid]
            qhlimit = growingInfo['qhlimit']/2#宠物强化上限
            if petInfo[typeStr] == qhlimit:
                return {'result':False,'message':Lg().g(1003)}
            needMoney = intensify_table.get(petInfo[typeStr]+1)['money']#强化需要金钱
            if self._owner.silvermoney < needMoney:
                return {'result':False,'message':Lg().g(1004)}
            probability = intensify_table.get(petInfo[typeStr]+1)['probability']#强化成功几率
            intensifyResult = True
            resultStr = "成功"
            addHp = 0
            hpMoney = 1
            addAtt = 0
            attMoney = 1
            djMoney = 1
            newFighting = 0
            newColor = 0
            newLevelLimit = 1
            if random.randint(1,100) > probability:
                intensifyResult = False
                resultStr = "失败"
                self.luck += 10#幸运值+10
                playerInfo['luck'] = self.luck
            else:
                petInfo[typeStr] += 1#强化等级+1
                if typeId == 1:
                    addHp = self.countAddAtt(pet.gid, typeId, petInfo[typeStr])
                    hpMoney = 0
                    if petInfo[typeStr] != qhlimit:
                        hpMoney = intensify_table.get(petInfo[typeStr]+1)['money']
                else:
                    addAtt = self.countAddAtt(pet.gid, typeId, petInfo[typeStr])
                    attMoney = 0
                    if petInfo[typeStr] != qhlimit:
                        attMoney = intensify_table.get(petInfo[typeStr]+1)['money']
                newFighting = self._owner.countFn()
                petInfoObj.update_multi(petInfo)
                petInfoObj.syncDB()
                self.luck = 0#幸运值清零
                playerInfo['luck'] = self.luck
            self._owner.silvermoney -= needMoney
            playerInfo['silvermoney'] -= needMoney
            playerInfoObj.update_multi(playerInfo)
            message = Lg().g(1005) % resultStr
            data = {"intensifyResult":intensifyResult,"money":needMoney,"addhp":addHp,"hpmoney":hpMoney,"addatt":addAtt,"attmoney":attMoney,\
                    "djmoney":djMoney,"newfighting":newFighting,"newcolor":newColor,"newlevellimit":newLevelLimit,"luck":self.luck}
            return {'result':True,'message':message,'data':data}
        if typeId == 3:#渡劫
            if pet.gid == 4:#顶级宠物
                return {'result':False,'message':Lg().g(1007)}
            from app.scense.tables import intensify_table,growing_table
            growingInfo = growing_table[pet.gid]
            qhlimit = growingInfo['qhlimit']/2
            petInfoObj = playerPet_m.getObj(petId)
            petInfo = petInfoObj.get("data")
            if petInfo['qhhplv'] != qhlimit or petInfo['qhhplv'] != qhlimit:
                return {'result':False,'message':Lg().g(1006)}
            needMoney = growingInfo['upmoney']
            if self._owner.silvermoney < needMoney:
                return {'result':False,'message':Lg().g(1004)}
            self._owner.silvermoney -= needMoney
            playerInfo['silvermoney'] -= needMoney
            playerInfoObj.update_multi(playerInfo)
            petInfo['newgid'] = pet.gid + 1
            petInfoObj.update_multi(petInfo)
            #petInfoObj.syncDB()#
            addHp = self.countDJAdd(pet.gid,petInfo["qhhplv"], 1)
            hpMoney = intensify_table.get(petInfo["qhhplv"]+1)['money']
            addAtt = self.countDJAdd(pet.gid,petInfo["qhattlv"], 2)
            attMoney = intensify_table.get(petInfo["qhattlv"]+1)['money']
            newGrowingInfo = growing_table[pet.gid+1]
            djMoney = newGrowingInfo['upmoney']
            newFighting = self._owner.countFn()
            newColor = newGrowingInfo['color']
            newLevelLimit = newGrowingInfo['qhlimit']
            data = {"intensifyResult":True,"money":needMoney,"addhp":addHp,"hpmoney":hpMoney,"addatt":addAtt,"attmoney":attMoney,\
                    "djmoney":djMoney,"newfighting":newFighting,"newcolor":newColor,"newlevellimit":newLevelLimit,"luck":self.luck}
            return {'result':True,'message':Lg().g(1008),'data':data}
                
